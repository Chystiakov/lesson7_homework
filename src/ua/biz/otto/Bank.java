package ua.biz.otto;

public class Bank extends Finance {
    public Bank(String name, double course) {
        super(name, course, 150_000);
    }

    public double convert(double uah) {
        return super.convert(uah) * 0.95;
    }
}
