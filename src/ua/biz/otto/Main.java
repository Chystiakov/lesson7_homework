package ua.biz.otto;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here

        Finance[] finances = new Finance[6];


        Scanner scanner = new Scanner(System.in);
        System.out.println("Input amount: ");
        double uah = scanner.nextDouble();

        finances[0] = new Bank("Privat", 26.6);
        finances[1] = new Bank("Pumb", 26.5);
        finances[2] = new Bank("OTP", 26.4);

        finances[3] = new Exchanger("ex1", 26.3, 5000);
        finances[4] = new Exchanger("ex2", 26.2, 4000);
        finances[5] = new Exchanger("ex3", 26.1, 3000);

        //finances[6] = new BlackMarket("BlackMarket", 26.0, );

        // Banks convert
        for (int i = 0; i < finances.length; i++) {
            if (finances[i].convert(uah) > finances[i].limit) {
                System.out.println(finances[i].name + " Overlimit: " + finances[i].limit);
            } else {
                System.out.println(finances[i].name + " = " + finances[i].convert(uah));
            }
        }


    }
}
