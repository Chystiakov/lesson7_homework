package ua.biz.otto;

public class Finance {

    public String name;
    public double course;
    public double uah;
    public double limit;

    public Finance(String name, double course, double limit) {
        this.name = name;
        this.course = course;
        this.limit = limit;
    }


    public double convert(double uah) {
        return (uah / course);
    }
}